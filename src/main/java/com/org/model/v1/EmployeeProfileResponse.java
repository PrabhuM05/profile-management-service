package com.org.model.v1;

/*import com.myorg.base.objects.CommonStatus;
import com.myorg.base.objects.Response;*/

import lombok.Data;

@Data
public class EmployeeProfileResponse/* implements Response<CommonStatus> */{
	
	
	//private CommonStatus status=new CommonStatus() ;
	private String empId;
	private String email;
	private String fullName;
	private String mobileNumber;
	

	
}
